using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http.Json;

namespace BlankXmlParserDemo
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string path = Directory.GetCurrentDirectory() + @"\SAPUNY2.xml";
                string xmlInputData = File.ReadAllText(path);
                string[] nameOfClassesForMapping = new string[] { "HotelSEinkauf" };

                Hotelstamm xml = Serializer.XmlDeserialize<Hotelstamm>(xmlInputData);
                var hotelSeinkaufList = ElementMapper.Mapper<HotelSEinkauf>(xml.Hotel.HotelBSeite.HotelSaisonzeiten.HotelSEinkauf);
                var hotelEinkaufspreise = ElementMapper.Mapper<HotelEinkaufspreise>(xml.Hotel.HotelBSeite.HotelEinkaufspreise, new string[] { "HotelESaisonzeit", "HotelEBruttoEinkauf", "HotelEProvision", "HotelEEinkaufspreis", "HotelEVerkauf" });
                Console.WriteLine($"hotelSeinkaufList: {JsonConvert.SerializeObject(hotelSeinkaufList)}");
                Console.WriteLine($"hotelEinkaufspreise: {JsonConvert.SerializeObject(hotelEinkaufspreise)}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
