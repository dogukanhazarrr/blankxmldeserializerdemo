﻿using System.IO;
using System.Xml.Serialization;

namespace BlankXmlParserDemo
{
        public class Serializer
        {
            /// <summary>
            /// populate a class with xml data 
            /// </summary>
            /// <typeparam name="T">Object Type</typeparam>
            /// <param name="input">xml data</param>
            /// <returns>Object Type</returns>
            public static T XmlDeserialize<T>(string input) where T : class, new()
            {
                XmlSerializer ser = new(typeof(T));

            using StringReader sr = new(input);
            return (T)ser.Deserialize(sr);
        }

            /// <summary>
            /// convert object to xml string
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="ObjectToSerialize"></param>
            /// <returns></returns>
            public static string XmlSerialize<T>(T ObjectToSerialize)
            {
            XmlSerializer xmlSerializer = new(ObjectToSerialize.GetType());
            using StringWriter textWriter = new();
            xmlSerializer.Serialize(textWriter, ObjectToSerialize);
            return textWriter.ToString();
        }
    }
}
