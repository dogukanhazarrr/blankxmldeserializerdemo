﻿using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using System.Dynamic;

namespace BlankXmlParserDemo
{
    public class ElementMapper
    {
        public static List<T> Mapper<T>(T element, string[] nameOfPropsForMapping = null, string seperator = "|") where T : class
        {
            nameOfPropsForMapping ??= Array.Empty<string>();
            List<T> mappedList = new();
            PropertyInfo[] props = element
                .GetType()
                .GetProperties()
                .Where(p => nameOfPropsForMapping.Length == 0 || nameOfPropsForMapping.Contains(p.Name))
                .ToArray();
            Dictionary<string, string[]> propsDict = props
                .ToDictionary(prop => prop.Name, prop => prop.GetValue(element, null).ToString().Split(seperator, StringSplitOptions.TrimEntries));

            foreach (var (prop, i) in props.Select((v, i) => (v, i)))
            {
                string key = prop.Name;
                string[] values = propsDict[key];
                bool isFirstMapping = mappedList.Count < values.Length;
                foreach (var (val, j) in values.Select((v, i) => (v, i)))
                {
                    if (isFirstMapping)
                    {
                        var newMapped = Activator.CreateInstance(typeof(T));
                        prop.SetValue(newMapped, val);
                        mappedList.Add(newMapped as T);
                    }
                    else
                    {
                        var mapped = mappedList[j];
                        prop.SetValue(mapped, val);
                    }
                }
            }

            return mappedList;
        }
        public static List<List<T>> Mapper<T>(List<T> elements, string[] nameOfPropsForMapping = null, string seperator = "|") where T : class
        {
            nameOfPropsForMapping ??= Array.Empty<string>();
            List<List<T>> mappedListInList = new();
            foreach (var element in elements)
            {
                List<T> mappedList = new();
                PropertyInfo[] props = element
               .GetType()
               .GetProperties()
               .Where(p => nameOfPropsForMapping.Length == 0 || nameOfPropsForMapping.Contains(p.Name))
               .ToArray();
                Dictionary<string, string[]> propsDict = props
                    .ToDictionary(prop => prop.Name, prop => prop.GetValue(element, null).ToString().Split(seperator, StringSplitOptions.TrimEntries));

                foreach (var (prop, i) in props.Select((v, i) => (v, i)))
                {
                    string key = prop.Name;
                    string[] values = propsDict[key];
                    bool isFirstMapping = mappedList.Count < values.Length;
                    foreach (var (val, j) in values.Select((v, i) => (v, i)))
                    {
                        if (isFirstMapping)
                        {
                            var newMapped = Activator.CreateInstance(typeof(T));
                            prop.SetValue(newMapped, val);
                            mappedList.Add(newMapped as T);
                        }
                        else
                        {
                            var mapped = mappedList[j];
                            prop.SetValue(mapped, val);
                        }
                    }
                }
                mappedListInList.Add(mappedList);
            }

            return mappedListInList;
        }
    }
}
