using System.Collections.Generic;
using System.Xml.Serialization;
namespace BlankXmlParserDemo
{
    [XmlRoot(ElementName = "Hotelstamm")]
    public class Hotelstamm
    {

        [XmlElement(ElementName = "SchalterE")]
        public object SchalterE { get; set; }

        [XmlElement(ElementName = "Hotel")]
        public Hotel Hotel { get; set; }
    }

    [XmlRoot(ElementName = "Hotel")]
    public class Hotel
    {

        [XmlElement(ElementName = "HotelBezeichnung")]
        public string HotelBezeichnung { get; set; }

        [XmlElement(ElementName = "HotelAgent")]
        public object HotelAgent { get; set; }

        [XmlElement(ElementName = "HotelAllgemein")]
        public HotelAllgemein HotelAllgemein { get; set; }

        [XmlElement(ElementName = "HotelBSeite")]
        public HotelBSeite HotelBSeite { get; set; }
    }

    [XmlRoot(ElementName = "HotelAllgemein")]
    public class HotelAllgemein
    {

        [XmlElement(ElementName = "HotelAZielcode1")]
        public string HotelAZielcode1 { get; set; }

        [XmlElement(ElementName = "HotelAZielcode2")]
        public object HotelAZielcode2 { get; set; }

        [XmlElement(ElementName = "HotelAZielcode2Kenz")]
        public object HotelAZielcode2Kenz { get; set; }

        [XmlElement(ElementName = "HotelAZielcode3")]
        public object HotelAZielcode3 { get; set; }

        [XmlElement(ElementName = "HotelAZielcode3Kenz")]
        public object HotelAZielcode3Kenz { get; set; }

        [XmlElement(ElementName = "HotelAKombiCode")]
        public object HotelAKombiCode { get; set; }

        [XmlElement(ElementName = "HotelAKombiCodeEnd")]
        public object HotelAKombiCodeEnd { get; set; }

        [XmlElement(ElementName = "HotelAZentralagent")]
        public string HotelAZentralagent { get; set; }

        [XmlElement(ElementName = "HotelABhfLandSitz")]
        public object HotelABhfLandSitz { get; set; }

        [XmlElement(ElementName = "HotelABhfKurzSitz")]
        public object HotelABhfKurzSitz { get; set; }

        [XmlElement(ElementName = "HotelABhfLandLiege")]
        public object HotelABhfLandLiege { get; set; }

        [XmlElement(ElementName = "HotelABhfKurzLiege")]
        public object HotelABhfKurzLiege { get; set; }

        [XmlElement(ElementName = "HotelAZahlungsEmpf")]
        public object HotelAZahlungsEmpf { get; set; }

        [XmlElement(ElementName = "HotelAOrt1")]
        public string HotelAOrt1 { get; set; }

        [XmlElement(ElementName = "HotelARegion")]
        public object HotelARegion { get; set; }

        [XmlElement(ElementName = "HotelAName")]
        public string HotelAName { get; set; }

        [XmlElement(ElementName = "HotelABeschreibung")]
        public string HotelABeschreibung { get; set; }

        [XmlElement(ElementName = "HotelAKategorieInt")]
        public string HotelAKategorieInt { get; set; }

        [XmlElement(ElementName = "HotelASterne")]
        public string HotelASterne { get; set; }

        [XmlElement(ElementName = "HotelAAlternative")]
        public object HotelAAlternative { get; set; }

        [XmlElement(ElementName = "HotelAPrio")]
        public string HotelAPrio { get; set; }

        [XmlElement(ElementName = "HotelARLZiel")]
        public object HotelARLZiel { get; set; }

        [XmlElement(ElementName = "HotelAProdukt")]
        public object HotelAProdukt { get; set; }

        [XmlElement(ElementName = "HotelAIntBem")]
        public object HotelAIntBem { get; set; }

        [XmlElement(ElementName = "HotelATyp")]
        public string HotelATyp { get; set; }

        [XmlElement(ElementName = "HotelASprache")]
        public string HotelASprache { get; set; }

        [XmlElement(ElementName = "HotelAAvis")]
        public object HotelAAvis { get; set; }

        [XmlElement(ElementName = "HotelAVersKenz")]
        public object HotelAVersKenz { get; set; }

        [XmlElement(ElementName = "HotelACetsArt")]
        public object HotelACetsArt { get; set; }

        [XmlElement(ElementName = "HotelAHotliOpt")]
        public object HotelAHotliOpt { get; set; }

        [XmlElement(ElementName = "HotelAHotliGrp")]
        public object HotelAHotliGrp { get; set; }

        [XmlElement(ElementName = "HotelABuchKto")]
        public string HotelABuchKto { get; set; }

        [XmlElement(ElementName = "HotelABuchKtoDatum")]
        public object HotelABuchKtoDatum { get; set; }

        [XmlElement(ElementName = "HotelABuchKtoNeu")]
        public object HotelABuchKtoNeu { get; set; }

        [XmlElement(ElementName = "HotelAUmsKto")]
        public object HotelAUmsKto { get; set; }

        [XmlElement(ElementName = "HotelALieferantTyp")]
        public object HotelALieferantTyp { get; set; }

        [XmlElement(ElementName = "HotelAName1Adr")]
        public string HotelAName1Adr { get; set; }

        [XmlElement(ElementName = "HotelAName2Adr")]
        public object HotelAName2Adr { get; set; }

        [XmlElement(ElementName = "HotelAManager")]
        public object HotelAManager { get; set; }

        [XmlElement(ElementName = "HotelAStrasse")]
        public string HotelAStrasse { get; set; }

        [XmlElement(ElementName = "HotelAPostfach")]
        public object HotelAPostfach { get; set; }

        [XmlElement(ElementName = "HotelAPlz")]
        public string HotelAPlz { get; set; }

        [XmlElement(ElementName = "HotelAOrt")]
        public string HotelAOrt { get; set; }

        [XmlElement(ElementName = "HotelALand")]
        public string HotelALand { get; set; }

        [XmlElement(ElementName = "HotelATelefon1")]
        public string HotelATelefon1 { get; set; }

        [XmlElement(ElementName = "HotelATelefon2")]
        public object HotelATelefon2 { get; set; }

        [XmlElement(ElementName = "HotelAIntInfo1")]
        public object HotelAIntInfo1 { get; set; }

        [XmlElement(ElementName = "HotelAIntInfo2")]
        public object HotelAIntInfo2 { get; set; }

        [XmlElement(ElementName = "HotelAFax1")]
        public string HotelAFax1 { get; set; }

        [XmlElement(ElementName = "HotelAFax2")]
        public object HotelAFax2 { get; set; }

        [XmlElement(ElementName = "HotelATeilkontiBez")]
        public object HotelATeilkontiBez { get; set; }

        [XmlElement(ElementName = "HotelATeilkontiAg")]
        public object HotelATeilkontiAg { get; set; }

        [XmlElement(ElementName = "HotelAHaupthotel")]
        public string HotelAHaupthotel { get; set; }
    }

    [XmlRoot(ElementName = "HotelSEinkauf")]
    public class HotelSEinkauf
    {

        [XmlElement(ElementName = "HotelSEinkaufVon")]
        public string HotelSEinkaufVon { get; set; }

        [XmlElement(ElementName = "HotelSEinkaufTyp")]
        public string HotelSEinkaufTyp { get; set; }
    }

    [XmlRoot(ElementName = "HotelSKontingente")]
    public class HotelSKontingente
    {

        [XmlElement(ElementName = "HotelSKontingenteVon")]
        public string HotelSKontingenteVon { get; set; }

        [XmlElement(ElementName = "HotelSKontingenteTyp")]
        public string HotelSKontingenteTyp { get; set; }
    }

    [XmlRoot(ElementName = "HotelSVerkauf")]
    public class HotelSVerkauf
    {

        [XmlElement(ElementName = "HotelSVerkaufVon")]
        public string HotelSVerkaufVon { get; set; }

        [XmlElement(ElementName = "HotelSVerkaufTyp")]
        public string HotelSVerkaufTyp { get; set; }
    }

    [XmlRoot(ElementName = "HotelSaisonzeiten")]
    public class HotelSaisonzeiten
    {

        [XmlElement(ElementName = "HotelSEinkauf")]
        public HotelSEinkauf HotelSEinkauf { get; set; }

        [XmlElement(ElementName = "HotelSKontingente")]
        public HotelSKontingente HotelSKontingente { get; set; }

        [XmlElement(ElementName = "HotelSVerkauf")]
        public HotelSVerkauf HotelSVerkauf { get; set; }
    }

    [XmlRoot(ElementName = "HotelKontingente")]
    public class HotelKontingente
    {

        [XmlElement(ElementName = "HotelKZimmercode")]
        public string HotelKZimmercode { get; set; }

        [XmlElement(ElementName = "HotelKZimmerbezeichnung")]
        public string HotelKZimmerbezeichnung { get; set; }

        [XmlElement(ElementName = "HotelKZimmerZusatztext")]
        public object HotelKZimmerZusatztext { get; set; }

        [XmlElement(ElementName = "HotelKPersonen")]
        public string HotelKPersonen { get; set; }

        [XmlElement(ElementName = "HotelKVollzahlerMin")]
        public string HotelKVollzahlerMin { get; set; }

        [XmlElement(ElementName = "HotelKVollzahlerMax")]
        public string HotelKVollzahlerMax { get; set; }

        [XmlElement(ElementName = "HotelKMinimalbelegung")]
        public string HotelKMinimalbelegung { get; set; }

        [XmlElement(ElementName = "HotelKMaximalbelegung")]
        public string HotelKMaximalbelegung { get; set; }

        [XmlElement(ElementName = "HotelKMinimalKenz")]
        public string HotelKMinimalKenz { get; set; }

        [XmlElement(ElementName = "HotelKMinimalbelegungErw")]
        public string HotelKMinimalbelegungErw { get; set; }

        [XmlElement(ElementName = "HotelKMaximalbelegungErw")]
        public string HotelKMaximalbelegungErw { get; set; }

        [XmlElement(ElementName = "HotelKMinBelegErwFHKA")]
        public object HotelKMinBelegErwFHKA { get; set; }

        [XmlElement(ElementName = "HotelKMaxBelegErwFHKA")]
        public object HotelKMaxBelegErwFHKA { get; set; }

        [XmlElement(ElementName = "HotelKExternBuchbar")]
        public object HotelKExternBuchbar { get; set; }

        [XmlElement(ElementName = "HotelKInfantSteuerung")]
        public object HotelKInfantSteuerung { get; set; }

        [XmlElement(ElementName = "HotelKKinderSperrVon")]
        public string HotelKKinderSperrVon { get; set; }

        [XmlElement(ElementName = "HotelKKinderSperrBis")]
        public string HotelKKinderSperrBis { get; set; }

        [XmlElement(ElementName = "HotelKVerpflegung")]
        public string HotelKVerpflegung { get; set; }

        [XmlElement(ElementName = "HotelKZimmertabnr")]
        public string HotelKZimmertabnr { get; set; }

        [XmlElement(ElementName = "HotelKZimmertabKenz")]
        public string HotelKZimmertabKenz { get; set; }

        [XmlElement(ElementName = "HotelKZimmerKoeff")]
        public object HotelKZimmerKoeff { get; set; }

        [XmlElement(ElementName = "HotelKZimmertabnr2")]
        public object HotelKZimmertabnr2 { get; set; }

        [XmlElement(ElementName = "HotelKZimmerKoeff2")]
        public object HotelKZimmerKoeff2 { get; set; }

        [XmlElement(ElementName = "HotelKZimmeranzahl")]
        public string HotelKZimmeranzahl { get; set; }

        [XmlElement(ElementName = "HotelKKinderAnzahl")]
        public string HotelKKinderAnzahl { get; set; }

        [XmlElement(ElementName = "HotelKKinderAlter")]
        public string HotelKKinderAlter { get; set; }

        [XmlElement(ElementName = "HotelKEMob")]
        public object HotelKEMob { get; set; }

        [XmlElement(ElementName = "HotelKOtdsZimTyp")]
        public object HotelKOtdsZimTyp { get; set; }

        [XmlElement(ElementName = "HotelKOTDSBuch")]
        public object HotelKOTDSBuch { get; set; }

        [XmlElement(ElementName = "HotelKMeerblick")]
        public object HotelKMeerblick { get; set; }

        [XmlElement(ElementName = "HotelKZusatzLeistung")]
        public object HotelKZusatzLeistung { get; set; }

        [XmlElement(ElementName = "HotelKSto9")]
        public object HotelKSto9 { get; set; }
    }

    [XmlRoot(ElementName = "HotelEinkaufspreise")]
    public class HotelEinkaufspreise
    {

        [XmlElement(ElementName = "HotelETyp")]
        public object HotelETyp { get; set; }

        [XmlElement(ElementName = "HotelEZimmercode")]
        public string HotelEZimmercode { get; set; }

        [XmlElement(ElementName = "HotelEZimmerbezeichnung")]
        public string HotelEZimmerbezeichnung { get; set; }

        [XmlElement(ElementName = "HotelEVerpflegung")]
        public string HotelEVerpflegung { get; set; }

        [XmlElement(ElementName = "HotelEZuweisung")]
        public object HotelEZuweisung { get; set; }

        [XmlElement(ElementName = "HotelEErmaessigung1")]
        public string HotelEErmaessigung1 { get; set; }

        [XmlElement(ElementName = "HotelEErmaessigung2")]
        public object HotelEErmaessigung2 { get; set; }

        [XmlElement(ElementName = "HotelEPreisberechnungsart")]
        public string HotelEPreisberechnungsart { get; set; }

        [XmlElement(ElementName = "HotelEPreistabelle")]
        public string HotelEPreistabelle { get; set; }

        [XmlElement(ElementName = "HotelEPreistabverweis")]
        public string HotelEPreistabverweis { get; set; }

        [XmlElement(ElementName = "HotelEPauschalpreisTabnr")]
        public object HotelEPauschalpreisTabnr { get; set; }

        [XmlElement(ElementName = "HotelEZeitbezug")]
        public string HotelEZeitbezug { get; set; }

        [XmlElement(ElementName = "HotelESaisonzeit")]
        public string HotelESaisonzeit { get; set; }

        [XmlElement(ElementName = "HotelEBruttoEinkauf")]
        public string HotelEBruttoEinkauf { get; set; }

        [XmlElement(ElementName = "HotelEProvision")]
        public string HotelEProvision { get; set; }

        [XmlElement(ElementName = "HotelEEinkaufspreis")]
        public string HotelEEinkaufspreis { get; set; }

        [XmlElement(ElementName = "HotelEVerkauf")]
        public string HotelEVerkauf { get; set; }
    }

    [XmlRoot(ElementName = "HotelCErmaessigungen")]
    public class HotelCErmaessigungen
    {

        [XmlElement(ElementName = "HotelCTabelle")]
        public string HotelCTabelle { get; set; }

        [XmlElement(ElementName = "HotelCErmArt")]
        public string HotelCErmArt { get; set; }

        [XmlElement(ElementName = "HotelCVKErmArt")]
        public string HotelCVKErmArt { get; set; }

        [XmlElement(ElementName = "HotelCZeitbezugEK")]
        public object HotelCZeitbezugEK { get; set; }

        [XmlElement(ElementName = "HotelCZeitbezugVK")]
        public object HotelCZeitbezugVK { get; set; }

        [XmlElement(ElementName = "HotelCKDauersonderpreis")]
        public object HotelCKDauersonderpreis { get; set; }

        [XmlElement(ElementName = "HotelCKVKDauersonderpreis")]
        public object HotelCKVKDauersonderpreis { get; set; }

        [XmlElement(ElementName = "HotelCAlter")]
        public string HotelCAlter { get; set; }

        [XmlElement(ElementName = "HotelCWert1")]
        public string HotelCWert1 { get; set; }

        [XmlElement(ElementName = "HotelCKErmArtWert1")]
        public string HotelCKErmArtWert1 { get; set; }

        [XmlElement(ElementName = "HotelCVKWert1")]
        public string HotelCVKWert1 { get; set; }

        [XmlElement(ElementName = "HotelCKVKErmArtWert1")]
        public string HotelCKVKErmArtWert1 { get; set; }

        [XmlElement(ElementName = "HotelCWert2")]
        public string HotelCWert2 { get; set; }

        [XmlElement(ElementName = "HotelCWert3")]
        public string HotelCWert3 { get; set; }

        [XmlElement(ElementName = "HotelCKErmArtWert2")]
        public string HotelCKErmArtWert2 { get; set; }

        [XmlElement(ElementName = "HotelCKErmArtWert3")]
        public string HotelCKErmArtWert3 { get; set; }
    }

    [XmlRoot(ElementName = "HotelNebenkosten")]
    public class HotelNebenkosten
    {

        [XmlElement(ElementName = "HotelNLfd")]
        public string HotelNLfd { get; set; }

        [XmlElement(ElementName = "HotelNKostencode")]
        public string HotelNKostencode { get; set; }

        [XmlElement(ElementName = "HotelNZimmercode")]
        public string HotelNZimmercode { get; set; }

        [XmlElement(ElementName = "HotelNVerpflegung")]
        public object HotelNVerpflegung { get; set; }

        [XmlElement(ElementName = "HotelNFix")]
        public string HotelNFix { get; set; }

        [XmlElement(ElementName = "HotelNVonGueltig")]
        public string HotelNVonGueltig { get; set; }

        [XmlElement(ElementName = "HotelNBisGueltig")]
        public string HotelNBisGueltig { get; set; }

        [XmlElement(ElementName = "HotelNZeitbezug")]
        public string HotelNZeitbezug { get; set; }

        [XmlElement(ElementName = "HotelNPersbezug")]
        public string HotelNPersbezug { get; set; }

        [XmlElement(ElementName = "HotelNPersbezugVonAlter")]
        public string HotelNPersbezugVonAlter { get; set; }

        [XmlElement(ElementName = "HotelNPersbezugBisAlter")]
        public string HotelNPersbezugBisAlter { get; set; }

        [XmlElement(ElementName = "HotelNAbPersanzahl")]
        public object HotelNAbPersanzahl { get; set; }

        [XmlElement(ElementName = "HotelNMindauer")]
        public string HotelNMindauer { get; set; }

        [XmlElement(ElementName = "HotelNMaxdauer")]
        public string HotelNMaxdauer { get; set; }

        [XmlElement(ElementName = "HotelNEinkaufspreis")]
        public string HotelNEinkaufspreis { get; set; }

        [XmlElement(ElementName = "HotelNAb3L")]
        public object HotelNAb3L { get; set; }

        [XmlElement(ElementName = "HotelNGueltigkeit")]
        public string HotelNGueltigkeit { get; set; }

        [XmlElement(ElementName = "HotelNVerkaufspreis")]
        public string HotelNVerkaufspreis { get; set; }

        [XmlElement(ElementName = "HotelNBeschreibung")]
        public string HotelNBeschreibung { get; set; }

        [XmlElement(ElementName = "HotelNWaehrung")]
        public string HotelNWaehrung { get; set; }

        [XmlElement(ElementName = "HotelNWaehrungKurz")]
        public object HotelNWaehrungKurz { get; set; }

        [XmlElement(ElementName = "HotelNWaehrungFaktor")]
        public string HotelNWaehrungFaktor { get; set; }

        [XmlElement(ElementName = "HotelNDru")]
        public object HotelNDru { get; set; }

        [XmlElement(ElementName = "HotelNProvision")]
        public string HotelNProvision { get; set; }

        [XmlElement(ElementName = "HotelNVoucher")]
        public string HotelNVoucher { get; set; }

        [XmlElement(ElementName = "HotelNAvis")]
        public object HotelNAvis { get; set; }

        [XmlElement(ElementName = "HotelNDruckKennzeichen")]
        public string HotelNDruckKennzeichen { get; set; }

        [XmlElement(ElementName = "HotelNExternBuchbar")]
        public string HotelNExternBuchbar { get; set; }

        [XmlElement(ElementName = "HotelNSteuerungPaket")]
        public string HotelNSteuerungPaket { get; set; }

        [XmlElement(ElementName = "HotelNPruefUmbDatum")]
        public string HotelNPruefUmbDatum { get; set; }

        [XmlElement(ElementName = "HotelNVonBuchdatum")]
        public object HotelNVonBuchdatum { get; set; }

        [XmlElement(ElementName = "HotelNBisBuchdatum")]
        public object HotelNBisBuchdatum { get; set; }

        [XmlElement(ElementName = "HotelNBeginnNK")]
        public string HotelNBeginnNK { get; set; }

        [XmlElement(ElementName = "HotelNEndeNK")]
        public string HotelNEndeNK { get; set; }

        [XmlElement(ElementName = "HotelNAgKette")]
        public string HotelNAgKette { get; set; }
    }

    [XmlRoot(ElementName = "HotelYZimmer")]
    public class HotelYZimmer
    {

        [XmlElement(ElementName = "HotelYZimmercode")]
        public object HotelYZimmercode { get; set; }

        [XmlElement(ElementName = "HotelYVerpflegung")]
        public object HotelYVerpflegung { get; set; }
    }

    [XmlRoot(ElementName = "HotelYEKSonderpreise")]
    public class HotelYEKSonderpreise
    {

        [XmlElement(ElementName = "HotelYVonGueltig")]
        public string HotelYVonGueltig { get; set; }

        [XmlElement(ElementName = "HotelYPreiscode")]
        public string HotelYPreiscode { get; set; }

        [XmlElement(ElementName = "HotelYBis")]
        public string HotelYBis { get; set; }

        [XmlElement(ElementName = "HotelYBeschreibung")]
        public string HotelYBeschreibung { get; set; }

        [XmlElement(ElementName = "HotelYIstAufenthalt")]
        public string HotelYIstAufenthalt { get; set; }

        [XmlElement(ElementName = "HotelYZahlAufenthalt")]
        public string HotelYZahlAufenthalt { get; set; }

        [XmlElement(ElementName = "HotelYMindauer")]
        public object HotelYMindauer { get; set; }

        [XmlElement(ElementName = "HotelYMaxdauer")]
        public string HotelYMaxdauer { get; set; }

        [XmlElement(ElementName = "HotelYGueltigkeit")]
        public string HotelYGueltigkeit { get; set; }

        [XmlElement(ElementName = "HotelYHoprozKalk")]
        public object HotelYHoprozKalk { get; set; }

        [XmlElement(ElementName = "HotelYZeitbezug")]
        public string HotelYZeitbezug { get; set; }

        [XmlElement(ElementName = "HotelYZimmer")]
        public HotelYZimmer HotelYZimmer { get; set; }

        [XmlElement(ElementName = "HotelYPersbezug")]
        public object HotelYPersbezug { get; set; }

        [XmlElement(ElementName = "HotelYPersArt")]
        public object HotelYPersArt { get; set; }

        [XmlElement(ElementName = "HotelYAgenturkette")]
        public object HotelYAgenturkette { get; set; }

        [XmlElement(ElementName = "HotelYAbflughafen3L")]
        public object HotelYAbflughafen3L { get; set; }

        [XmlElement(ElementName = "HotelYDruckKenz")]
        public string HotelYDruckKenz { get; set; }

        [XmlElement(ElementName = "HotelYPPTab")]
        public object HotelYPPTab { get; set; }

        [XmlElement(ElementName = "HotelYTageVorBeginn")]
        public object HotelYTageVorBeginn { get; set; }

        [XmlElement(ElementName = "HotelYPakEKKenz")]
        public object HotelYPakEKKenz { get; set; }

        [XmlElement(ElementName = "HotelYErmTab")]
        public object HotelYErmTab { get; set; }

        [XmlElement(ElementName = "HotelYHpl")]
        public object HotelYHpl { get; set; }

        [XmlElement(ElementName = "HotelYWertigkeit")]
        public string HotelYWertigkeit { get; set; }

        [XmlElement(ElementName = "HotelYWertigkeitBis")]
        public string HotelYWertigkeitBis { get; set; }

        [XmlElement(ElementName = "HotelYZuschlag")]
        public string HotelYZuschlag { get; set; }

        [XmlElement(ElementName = "HotelYPreis")]
        public string HotelYPreis { get; set; }

        [XmlElement(ElementName = "HotelYProzentKenz")]
        public string HotelYProzentKenz { get; set; }

        [XmlElement(ElementName = "HotelYTestUmbuchDatum")]
        public string HotelYTestUmbuchDatum { get; set; }

        [XmlElement(ElementName = "HotelYVonBuchungsdatum")]
        public object HotelYVonBuchungsdatum { get; set; }

        [XmlElement(ElementName = "HotelYBisBuchungsdatum")]
        public string HotelYBisBuchungsdatum { get; set; }

        [XmlElement(ElementName = "HotelYVonBuchUhrzeit")]
        public object HotelYVonBuchUhrzeit { get; set; }

        [XmlElement(ElementName = "HotelYBisBuchUhrzeit")]
        public object HotelYBisBuchUhrzeit { get; set; }

        [XmlElement(ElementName = "HotelYWoTage")]
        public object HotelYWoTage { get; set; }

        [XmlElement(ElementName = "HotelYWoTageSteu")]
        public object HotelYWoTageSteu { get; set; }

        [XmlElement(ElementName = "HotelYVonAbreisedatum")]
        public object HotelYVonAbreisedatum { get; set; }

        [XmlElement(ElementName = "HotelYBisAbreisedatum")]
        public object HotelYBisAbreisedatum { get; set; }

        [XmlElement(ElementName = "HotelYSeniorSteuer")]
        public object HotelYSeniorSteuer { get; set; }

        [XmlElement(ElementName = "HotelYLinkNK")]
        public object HotelYLinkNK { get; set; }
    }

    [XmlRoot(ElementName = "HotelTZusHotel")]
    public class HotelTZusHotel
    {

        [XmlElement(ElementName = "HotelTZBez")]
        public object HotelTZBez { get; set; }

        [XmlElement(ElementName = "HotelTZAgent")]
        public object HotelTZAgent { get; set; }

        [XmlElement(ElementName = "HotelTZZimmerart")]
        public object HotelTZZimmerart { get; set; }

        [XmlElement(ElementName = "HotelTZLeistung")]
        public object HotelTZLeistung { get; set; }

        [XmlElement(ElementName = "HotelTZBeginn")]
        public object HotelTZBeginn { get; set; }

        [XmlElement(ElementName = "HotelTZEnde")]
        public object HotelTZEnde { get; set; }

        [XmlElement(ElementName = "HotelTZRueckreise")]
        public object HotelTZRueckreise { get; set; }

        [XmlElement(ElementName = "HotelTZPauschal")]
        public object HotelTZPauschal { get; set; }

        [XmlElement(ElementName = "HotelTZPreiskenz")]
        public object HotelTZPreiskenz { get; set; }
    }

    [XmlRoot(ElementName = "HotelTSaisVerweis")]
    public class HotelTSaisVerweis
    {

        [XmlElement(ElementName = "HotelTSVEkGleichVk")]
        public object HotelTSVEkGleichVk { get; set; }
    }

    [XmlRoot(ElementName = "HotelTabellenVerweise")]
    public class HotelTabellenVerweise
    {

        [XmlElement(ElementName = "HotelTText1")]
        public object HotelTText1 { get; set; }

        [XmlElement(ElementName = "HotelTText2")]
        public object HotelTText2 { get; set; }

        [XmlElement(ElementName = "HotelTKatalogseite")]
        public object HotelTKatalogseite { get; set; }

        [XmlElement(ElementName = "HotelTKombiTab")]
        public object HotelTKombiTab { get; set; }

        [XmlElement(ElementName = "HotelTExterneBuchbarkeit")]
        public string HotelTExterneBuchbarkeit { get; set; }

        [XmlElement(ElementName = "HotelTOhneflug")]
        public string HotelTOhneflug { get; set; }

        [XmlElement(ElementName = "HotelTKette")]
        public object HotelTKette { get; set; }

        [XmlElement(ElementName = "HotelTOpt")]
        public object HotelTOpt { get; set; }

        [XmlElement(ElementName = "HotelTLm")]
        public object HotelTLm { get; set; }

        [XmlElement(ElementName = "HotelTDMX")]
        public object HotelTDMX { get; set; }

        [XmlElement(ElementName = "HotelTUeberschrift")]
        public string HotelTUeberschrift { get; set; }

        [XmlElement(ElementName = "HotelTZusHotel")]
        public HotelTZusHotel HotelTZusHotel { get; set; }

        [XmlElement(ElementName = "HotelTRueck3L")]
        public object HotelTRueck3L { get; set; }

        [XmlElement(ElementName = "HotelTMeldefrist")]
        public object HotelTMeldefrist { get; set; }

        [XmlElement(ElementName = "HotelTHandlingKto")]
        public object HotelTHandlingKto { get; set; }

        [XmlElement(ElementName = "HotelTHandlingKalk")]
        public object HotelTHandlingKalk { get; set; }

        [XmlElement(ElementName = "HotelT67")]
        public object HotelT67 { get; set; }

        [XmlElement(ElementName = "HotelTProv")]
        public string HotelTProv { get; set; }

        [XmlElement(ElementName = "HotelTPreisKenz")]
        public object HotelTPreisKenz { get; set; }

        [XmlElement(ElementName = "HotelTAnreisetag")]
        public object HotelTAnreisetag { get; set; }

        [XmlElement(ElementName = "HotelTFakturierung")]
        public object HotelTFakturierung { get; set; }

        [XmlElement(ElementName = "HotelTKontiDarst")]
        public object HotelTKontiDarst { get; set; }

        [XmlElement(ElementName = "HotelTWoche")]
        public object HotelTWoche { get; set; }

        [XmlElement(ElementName = "HotelTSaisVerweis")]
        public HotelTSaisVerweis HotelTSaisVerweis { get; set; }

        [XmlElement(ElementName = "HotelTHPL")]
        public object HotelTHPL { get; set; }

        [XmlElement(ElementName = "HotelTKenz")]
        public object HotelTKenz { get; set; }

        [XmlElement(ElementName = "HotelTSteuerngKind")]
        public object HotelTSteuerngKind { get; set; }

        [XmlElement(ElementName = "HotelTRechDruck")]
        public object HotelTRechDruck { get; set; }

        [XmlElement(ElementName = "HotelTHProvKto")]
        public object HotelTHProvKto { get; set; }

        [XmlElement(ElementName = "HotelTHProvKalk")]
        public object HotelTHProvKalk { get; set; }

        [XmlElement(ElementName = "HotelTExtVak")]
        public string HotelTExtVak { get; set; }

        [XmlElement(ElementName = "HotelTVoucherdruck")]
        public object HotelTVoucherdruck { get; set; }
    }

    [XmlRoot(ElementName = "HotelPKKontingent")]
    public class HotelPKKontingent
    {

        [XmlElement(ElementName = "HotelPKKSaisonzeit")]
        public string HotelPKKSaisonzeit { get; set; }

        [XmlElement(ElementName = "HotelPKKMaximal")]
        public string HotelPKKMaximal { get; set; }

        [XmlElement(ElementName = "HotelPKKExclusiv")]
        public string HotelPKKExclusiv { get; set; }

        [XmlElement(ElementName = "HotelPKKVerfall")]
        public string HotelPKKVerfall { get; set; }
    }

    [XmlRoot(ElementName = "HotelPoolKontingente")]
    public class HotelPoolKontingente
    {

        [XmlElement(ElementName = "HotelPKMarke")]
        public string HotelPKMarke { get; set; }

        [XmlElement(ElementName = "HotelPKServer")]
        public string HotelPKServer { get; set; }

        [XmlElement(ElementName = "HotelPKZimmercode")]
        public string HotelPKZimmercode { get; set; }

        [XmlElement(ElementName = "HotelPKExternZimmercode")]
        public string HotelPKExternZimmercode { get; set; }

        [XmlElement(ElementName = "HotelPKKontingent")]
        public List<HotelPKKontingent> HotelPKKontingent { get; set; }
    }

    [XmlRoot(ElementName = "HotelWeitereVerweise")]
    public class HotelWeitereVerweise
    {

        [XmlElement(ElementName = "HotelWVMaxKindalter")]
        public string HotelWVMaxKindalter { get; set; }

        [XmlElement(ElementName = "HotelWSeniorenalter")]
        public string HotelWSeniorenalter { get; set; }

        [XmlElement(ElementName = "HotelWFlugErm")]
        public string HotelWFlugErm { get; set; }

        [XmlElement(ElementName = "HotelWhpr-wt")]
        public string HotelWhprwt { get; set; }

        [XmlElement(ElementName = "HotelWhpr-erm")]
        public string HotelWhprerm { get; set; }
    }

    [XmlRoot(ElementName = "HotelBSeite")]
    public class HotelBSeite
    {

        [XmlElement(ElementName = "HotelBSaisonbereich")]
        public string HotelBSaisonbereich { get; set; }

        [XmlElement(ElementName = "HotelBSaisonVondatum")]
        public string HotelBSaisonVondatum { get; set; }

        [XmlElement(ElementName = "HotelBSaisonBisdatum")]
        public string HotelBSaisonBisdatum { get; set; }

        [XmlElement(ElementName = "HotelBSaisonVkWaehrung")]
        public string HotelBSaisonVkWaehrung { get; set; }

        [XmlElement(ElementName = "HotelBSaisonVkIsoCode")]
        public string HotelBSaisonVkIsoCode { get; set; }

        [XmlElement(ElementName = "HotelBSaisonEkWaehrung")]
        public string HotelBSaisonEkWaehrung { get; set; }

        [XmlElement(ElementName = "HotelBSaisonEkIsoCode")]
        public string HotelBSaisonEkIsoCode { get; set; }

        [XmlElement(ElementName = "HotelBSaisonEkFaktor")]
        public string HotelBSaisonEkFaktor { get; set; }

        [XmlElement(ElementName = "HotelBSaisonTax")]
        public string HotelBSaisonTax { get; set; }

        [XmlElement(ElementName = "HotelBSaisonTaxkenz")]
        public object HotelBSaisonTaxkenz { get; set; }

        [XmlElement(ElementName = "HotelBSaisonKalkTab")]
        public object HotelBSaisonKalkTab { get; set; }

        [XmlElement(ElementName = "HotelBSaisonKalkTabZu")]
        public object HotelBSaisonKalkTabZu { get; set; }

        [XmlElement(ElementName = "HotelSaisonzeiten")]
        public HotelSaisonzeiten HotelSaisonzeiten { get; set; }

        [XmlElement(ElementName = "HotelKontingente")]
        public List<HotelKontingente> HotelKontingente { get; set; }

        [XmlElement(ElementName = "HotelEinkaufspreise")]
        public List<HotelEinkaufspreise> HotelEinkaufspreise { get; set; }

        [XmlElement(ElementName = "HotelCErmaessigungen")]
        public List<HotelCErmaessigungen> HotelCErmaessigungen { get; set; }

        [XmlElement(ElementName = "HotelNebenkosten")]
        public List<HotelNebenkosten> HotelNebenkosten { get; set; }

        [XmlElement(ElementName = "HotelYEKSonderpreise")]
        public List<HotelYEKSonderpreise> HotelYEKSonderpreise { get; set; }

        [XmlElement(ElementName = "HotelTabellenVerweise")]
        public HotelTabellenVerweise HotelTabellenVerweise { get; set; }

        [XmlElement(ElementName = "HotelPoolKontingente")]
        public List<HotelPoolKontingente> HotelPoolKontingente { get; set; }

        [XmlElement(ElementName = "HotelWeitereVerweise")]
        public HotelWeitereVerweise HotelWeitereVerweise { get; set; }

        [XmlElement(ElementName = "HotelPruefZusLeistungen")]
        public object HotelPruefZusLeistungen { get; set; }

        [XmlElement(ElementName = "HotelPruefReiseziel")]
        public object HotelPruefReiseziel { get; set; }

        [XmlElement(ElementName = "HotelFPKTabelle")]
        public object HotelFPKTabelle { get; set; }

        [XmlElement(ElementName = "HotelTerminHinweise")]
        public object HotelTerminHinweise { get; set; }
    }
}